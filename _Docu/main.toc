\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {british}{}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Introduction}{i}{section.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Geometries}{1}{section.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}Material Parameters}{3}{section.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1}FR4}{3}{subsection.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2}Copper}{3}{subsection.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}Mathematical Description}{5}{section.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1}Simulation of the Electric Field }{5}{subsection.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.1.1}Weak Formulation}{6}{subsubsection.4.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2}Simulation of the Thermal Field}{8}{subsection.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.2.1}Weak Formulation}{8}{subsubsection.4.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3}Coupled Problem}{9}{subsection.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.4}The Melting Process}{10}{subsection.4.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.5}Solving the Non-Linearity}{11}{subsection.4.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.6}Solving the Time-Derivation}{12}{subsection.4.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.7}Solving the Stationary Equation}{12}{subsection.4.7}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5}Simulation Results}{14}{section.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1}Simulation of the Stationary Case}{14}{subsection.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2}Simulation of the Transient Case}{16}{subsection.5.2}
