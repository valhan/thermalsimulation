% !TeX spellcheck = en_GB
\section{Mathematical Description}
The electric flow field problem and the thermal diffusion problem are in general coupled via the thermal dependency of the electric conductivity. However, the problems can be derived independently and get coupled afterwards. 
\subsection{Simulation of the Electric Field }
The Maxwell equations of the electric flow field
\begin{subequations}
	\begin{align}
		\label{eq:Maxwell1}
		\nabla\times\textbf{E} &= \textbf{0} &&\textbf{E}_2\times\textbf{n}_2 + \textbf{E}_1\times\textbf{n}_1 = \textbf{0}\\
		\label{eq:Maxwell2}
		\nabla\cdot\textbf{J} &= \textbf{0} &&\textbf{J}_2\cdot\textbf{n}_2 + \textbf{J}_1\cdot\textbf{n}_1 = 0
	\end{align}
\end{subequations}
set the fundamental equations for the calculations. Ohm's law 
\begin{align}
\textbf{J} = \sigma\textbf{E}
\end{align}
reflects the material relation between electric field strength \textbf{E} and electric current density \textbf{J}. The parameter $\sigma$ is called electric conductivity. 
Since the rotation of the electric field strength vanishes (see \eqref{eq:Maxwell1}),  a scalar electric potential 
\begin{align}
	\textbf{E}  =  -\nabla\Phi
\end{align}
can be introduced. With the material relation and the scalar electric potential, the \gls{BVP} for the electric field problem
\begin{subequations}
\begin{align}
	-\nabla\cdot\big(\sigma\nabla\Phi) &= 0 \quad&&\textnormal{in }\Omega	\\
	\Phi &= \Phi_0 &&\textnormal{on }\Gamma_D\\
	\label{eq:Neumann}
	-\textbf{n}\cdot\sigma\nabla\Phi &= 0 &&\textnormal{on }\Gamma_N
\end{align}
\end{subequations}
can be derived. Dirichlet and Neumann boundaries are noted by $\Gamma_D$ and $\Gamma_N$, respectively.\\
To set defined states, the value of $\Phi_0$ is defined on both Dirichlet boundaries. Under consideration of a voltage $U_0$, the electric potential on the Dirichlet boundaries 
\begin{align}
	\Phi = \frac{U0}{2}\textnormal{ on }\Gamma_{D, left} \textnormal{ and } 	\Phi = -\frac{U0}{2}\textnormal{ on }\Gamma_{D, right} 
\end{align} 
are set.
Moreover, the local Ohm loss density 
\begin{align}
	\label{eq:elLosses}
	p = \textbf{J}\cdot\textbf{E}
\end{align}
can be calculated. The integration of the local Ohm losses over the whole copper
\begin{align}
	\label{eq:OhmLosses}
	P = \int_{\Omega_{CU}}p\;d\Omega
\end{align}
yields the overall Ohm losses of the problem. Further, a mean value of the electric current 
\begin{align}
	\label{eq:I}
	I = \frac{P}{U_0}
\end{align}
can be calculated. Ohm's global law yields the electric resistance
\begin{align}
	R = \frac{U_0}{I}.
\end{align}
 A cylinder with a length $l$, which is equal to the length of the unfolded geometry, would have the cross-sectional area 
 \begin{align}
 	A = \frac{Il}{\overline{\sigma_{CU}} U_0} \textnormal{, with } \overline{\sigma_{CU}} = \frac{1}{V}\int_{\Omega_{CU}}\sigma(\textbf{x})\;dV \textnormal{ and } V = \int_{\Omega_{CU}}1\;d\Omega.
 \end{align}
\subsubsection{Weak Formulation}
The weak formulation reduces the requirements for the solution of the field problem. 
For the weak formulation 
\begin{align}
-\int_\Omega\nabla\cdot\big(\sigma\nabla\Phi)\;v\;d\Omega = 0
\end{align}
the \gls{BVP} gets multiplied by a scalar test function $v$ and integrated over the area of interest  $\Omega$. With 
\begin{itemize}
	\item the partial integration rule $\nabla\cdot(\textbf{u}v) = \nabla\cdot(\textbf{u})v + \textbf{u}\cdot\nabla v$,
	\item the requirement for test functions to vanish at Dirichlet boundaries, 
	\item the Gauss theorem and 
	\item the homogenous Neumann boundary conditions \eqref{eq:Neumann}
\end{itemize} 
the final weak formulation
\begin{subequations}
\begin{align}
	\int_\Omega\sigma\nabla\Phi\cdot\nabla v\;d\Omega &= 0 \quad&&\textnormal{in }\Omega_{CU},\\
	\Phi &= \frac{U_0}{2} &&\textnormal{on }\Gamma_ {D, left},\\
	\Phi &= -\frac{U_0}{2} &&\textnormal{on }\Gamma_ {D, right},\\
	-\textbf{n}\cdot\sigma\nabla\Phi &= 0 &&\textnormal{on }\Gamma_N
\end{align}
\end{subequations}
is formed.  \cref{fig:ELJ1}, \cref{fig:ELJ2} and \cref{fig:ELJ3} depict the simulated current densities for three different geometries in the stationary case and $U_0 = 125$ mV. 
\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\linewidth]{Jgeo1}
	\caption{Vector plot with field lines of the simulated current density \textbf{J} for the first geometry and $U_0 = 125$ mV.}
	\label{fig:ELJ1}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\linewidth]{Jgeo2}
	\caption{Vector plot with field lines of the simulated current density \textbf{J} for the second geometry and $U_0 = 125$ mV..}
	\label{fig:ELJ2}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\linewidth]{Jgeo3}
	\caption{Vector plot with field lines of the simulated current density \textbf{J} for the third geometry and $U_0 = 125$ mV.}
	\label{fig:ELJ3}
\end{figure}

\subsection{Simulation of the Thermal Field}
The heat equation 
\begin{align}
	\nabla\cdot(\lambda\nabla T) = c\rho\frac{\partial}{\partial t}T - p\quad\textnormal{in } \Omega,
\end{align}
with its boundary conditions 
\begin{subequations}
\begin{align}
	\label{eq:TherBV1}
	T &= T_0 &&\textnormal{on }\Gamma_D\\
	\label{eq:TherBV2}
	-\textbf{n}\cdot(\lambda\nabla T) &= 0 &&\textnormal{on }\Gamma_N, 
\end{align}
\end{subequations}
sets the fundamental equation for the simulation \cite{Sabelka2001}. The heat equation is a diffusion equation and uses 
\begin{itemize}
	\item $\lambda$ as the thermal conductivity in $\frac{W}{m\;K}$, 
	\item $T$ as the temperature in $K$, 
	\item $c$ as the heat capacity  in $J/K$, 
	\item $\rho$ as the density of a material in $kg/m^3$ and 
	\item $p$ as external thermal source in $W/m^3$. 
\end{itemize}
\subsubsection{Weak Formulation}
\label{sub:ThermalWeak}
Analogously to the weak formulation of the electric field, the weak formulation of the heat equation 
\begin{align}
\int_\Omega\nabla\cdot(\lambda\nabla T)v - c\rho\frac{\partial}{\partial t}T\;v\;d\Omega = -\int_\Omega p\;v\;d\Omega
\end{align}
is built by multiplying with a scalar test function $v$ and by integrating over the full area $\Omega$. Additionally, known therms are moved to the left and unknowns to the right side of the equation. Partial integration of the first term yields
\begin{align}
-\int_\Omega\lambda\nabla T\cdot\nabla v \;d\Omega+ \int_\Omega\nabla\cdot(v\lambda\nabla T) \;d\Omega - \int_\Omega c\rho\frac{\partial}{\partial t}T\;v\;d\Omega = -\int_\Omega p\;v\;d\Omega.
\end{align}
Applying the Gauss theorem for the second term results in
\begin{align}
\int_\Omega\lambda\nabla T\cdot\nabla v \;d\Omega- \int_{\Gamma_N}\textbf{n}\cdot(\lambda\nabla T)v \;d\Omega + \int_\Omega c\rho\frac{\partial}{\partial t}T\;v\;d\Omega = \int_\Omega p\;v\;d\Omega,
\end{align}
where the vanishing of test functions at Dirichlet boundaries has been considered. Taking the homogenous Neumann boundary conditions into account ($T_t = 0$ in 	\eqref{eq:TherBV2}), the final weak formulation 
\begin{align}
\int_\Omega\lambda\nabla T\cdot\nabla v \;d\Omega + \frac{\partial}{\partial t}\int_\Omega c\rho T\;v\;d\Omega &= \int_\Omega p\;v\;d\Omega \quad&&\textnormal{in } \Omega,\\
	T &= T_0 &&\textnormal{on }\Gamma_D,\\
-\textbf{n}\cdot(\lambda\nabla T) &= 0 &&\textnormal{on }\Gamma_N
\end{align}
is derived, which considers a constant heat capacity $\frac{\partial}{\partial t} c = 0$ and a constant material density $\frac{\partial}{\partial t}\rho = 0$.

\subsection{Coupled Problem}
Beside influencing the thermal conductivity, the temperature influences the the electric conductivity too. Hence, the described problems are copled. The simulated temperature distribution is shown in \cref{fig:coupledT}. %Additionally, \cref{fig:coupledLambda} illustrates the thermal conductivity. It is clear, that the conductivity is lower in warmer regions. Analogously, \cref{fig:coupledSigma} displays the electric conductivity which decreases for warmer regions too. 
%The displayed results show the conditions after $3$s which indicate the stationary case. 
The set electric voltage between $\Gamma_{D,{left}}$ and $\Gamma_{D, right}$ is $U_0= 225$ mV. The maximal temperature is much lower than the melting temperature. Therefore, the track fuse has not been melted.  \\
A transient analysis for the temperature in the point $(x,y,z) = (0, 0, 0.008)$ mm and the current through the fuse is shown in \cref{fig:coupledTransient}. The current reaches a maximum value of approximately $1.95$A in the moment when the voltage is switched on. In the further progress, the copper temperature increases and therefore the conductivity of the copper decreases. Consequently, the current settles down at approximately $1.55$A. \\
The weak formulation of the coupled problem is described by
\begin{subequations}
	\label{eq:BVPCoupled}
\begin{align}
\int_\Omega\lambda(T)\nabla T\cdot\nabla v_T \;d\Omega + \frac{\partial}{\partial t}\int_\Omega c\rho T\;v_T\;d\Omega &= \int_\Omega p(T)\;v_T\;d\Omega\quad&&\textnormal{in } \Omega,\\
\int_{\Omega_{CU}}\sigma(T)\nabla\Phi\cdot\nabla v_E\;d\Omega &= 0 && \textnormal{in } \Omega_{CU},\\
T &= T_0 &&\textnormal{on }\Gamma_{T, D},\\
\Phi &= \frac{U_0}{2} &&\textnormal{on }\Gamma_{E, D{left}},\\
\Phi &= -\frac{U_0}{2} &&\textnormal{on }\Gamma_{E, D{right}},\\
-\textbf{n}\cdot(\lambda\nabla T) &= 0 &&\textnormal{on }\Gamma_{T, N},\\
-\textbf{n}\cdot\sigma\nabla\Phi &= 0 &&\textnormal{on }\Gamma_{E,N}. 
\end{align}
\end{subequations}
The boundaries using the subscript-letter $T$ describe thermal boundaries. Further, the subscript-letter $E$ indicates electric boundaries. \\
The voltage $U_0$ defines the electric behaviour of the flow field problem. The according location of the Dirichlet boundaries are outlined in \cref{fig:geo1} by the blue arrows. The temperature $T_0 = 25^\circ C$ reflects the initial thermal condition.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\linewidth]{coupledT2}
	\caption{Plot of the simulated temperature $T$ for the third geometry with $U_0=225mV$ and after $T_{end}=3s$, which indicates the stationary case.}
	\label{fig:coupledT}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\linewidth]{transientCurrentTemperature}
	\caption{Transient analysis of the temperature $T$ in the point $(x,y,z)=(0,0,0.009)$ mm and the current $I$ through the fuse. The set voltage is $U_0=225mV$, the final time is $T_{end}=3s$ and the third geometry are used.}
	\label{fig:coupledTransient}
\end{figure}
%\begin{figure}[H]
%	\centering
%	\includegraphics[width=0.8\linewidth]{coupledLambda}
%	\caption{Plot of the simulated thermal conductivity $\lambda$ for the third geometry with $U_0=225mV$ and $T_{end}=3s$.}
%	\label{fig:coupledLambda}
%\end{figure}
%\begin{figure}[H]
%	\centering
%	\includegraphics[width=0.8\linewidth]{coupledSigma}
%	\caption{Plot of the simulated electric conductivity $\sigma$ for the third geometry with $U_0=225mV$ and $T_{end}=3s$.}
%	\label{fig:coupledSigma}
%\end{figure}



\subsection{The Melting Process}
In this section the melting process of the copper should be modelled. With 
\begin{itemize}
	\item the molar enthalpy of fusion $H_{CU, fus}=13.05\frac{kJ}{mol}$, 
	\item the specific molar mass $m_{CU} = 63.54 \frac{g}{mol}$ and 
	\item the constant material density $\rho_{CU}=8.92\cdot10^3\frac{kg}{m^3}$ 
\end{itemize} the specific fusion energy density 
\begin{equation}
E_{CU, fus} = \frac{\rho_{CU}H_{CU, fus}}{m_{CU}} = 1832.011 \frac{MJ}{m^3}
\end{equation}
can be calculated. With the melting energy density
\begin{align}
E_{CU,heat} = c_{CU}\rho_{CU}(T - T_{melt}), 
\end{align}
the copper is considered to be melted if the heat energy
\begin{align}
E_{CU, heat} \ge E_{CU, fus}
\end{align}  
is higher than the fusion energy. The melting temperature of copper is $T_{melt} = 1084.62^\circ C$. The (virtual) simulation temperature $T$ can be  higher than the melting temperature. Therefore, the temperature $T$ is only used to enable the melting process. However, the real temperature of the melting material can not exceed the melting temperature while melting. Consequently, the real temperature is bounded by a penalty term.

\subsection{Solving the Non-Linearity}
The fixed-point method is selected for solving the non-linearity \cite{Kucz2008FP}.  The advantage of the fixed-point method is that by using the fixed-point parameters $\lambda_{FP}$ and $\sigma_{FP}$ the matrices of the \gls{FEM}-system have to be  assembled only once. According to \cite{Kucz2008FP}, the values of the fixed-point parameter are set as mean value of the maximal and minimal occurring values of the non-linear parameter during the simulation. The temperature in the simulation varies in the interval $[0, T_{max, CU}]$, with $T_{max, CU} = 1084.62^\circ C$ as melting temperature of copper. Therefore, the fixed-point values are set to 
\begin{align}
\lambda_{FP} = 332.5 \frac{W}{mK} \textnormal{ and } \sigma_{FP} = 37.452\frac{MS}{m}.
\end{align}
Applying the fixed-point method to the weak formulations of the coupled problem yields
\begin{subequations}
	\label{eq:BVPCoupledFP}
	\begin{align}
	\begin{split}
	\int_\Omega\lambda_{FP}\nabla T\cdot\nabla v_T \;d\Omega + \frac{\partial}{\partial t}\int_\Omega c\rho T\;v_T\;d\Omega &=\\ \int_\Omega\Big(\lambda_{FP} - \lambda(T)\Big)\nabla T^{k}\cdot\nabla v_T \;d\Omega &+ \int_\Omega p(T)\;v_T\;d\Omega\quad
	\end{split}
	 &&\textnormal{in } \Omega\\
	\int_{\Omega_{CU}}\sigma_{FP}\nabla\Phi\cdot\nabla v_E\;d\Omega &= \int_{\Omega_{CU}}\Big(\sigma_{FP} - \sigma(T)\Big)\nabla\Phi^k\nabla v_E && \textnormal{in } \Omega_{CU}\\
	T &= T_0 &&\textnormal{on }\Gamma_{T, D}\\
	\Phi &= \frac{U_0}{2} &&\textnormal{on }\Gamma_{E, D{left}}\\
	\Phi &= -\frac{U_0}{2} &&\textnormal{on }\Gamma_{E, D{right}}\\
	-\textbf{n}\cdot(\lambda\nabla T) &= 0 &&\textnormal{on }\Gamma_{T, N}\\
	-\textbf{n}\cdot\sigma\nabla\Phi &= 0 &&\textnormal{on }\Gamma_{E,N}. 
	\end{align}
\end{subequations}
When the fixed-point method is used, a previously solved solution $\Phi^k$ and $T^k$ is needed. In the first steps this solution is defined by the initial conditions. Further the subscript $E$ indicates the electric problem and $T$ the thermal one. 
\subsection{Solving the Time-Derivation}
With the \gls{FEM} an approximative solution \textbf{u}, which holds the the scalar electric potential $\Phi$ and the temperature $T$, is calculated. After assembling the \gls{FEM}-matrices and the \gls{FEM}-vector with
\begin{itemize}
	\item A as stiffness matrix, 
	\item M as mass matrix and 
	\item f as load vector
\end{itemize} the ordinary differential equation
\begin{align}
	\label{eq:TherFEMDiff}
	A \textbf{u} +\frac{\partial}{\partial t}M \textbf{u} = \textbf{f}
\end{align}
is solved by applying the implicit Euler method. The implicit Euler method 
\begin{align}
\frac{\partial}{\partial t} \textbf{u} \approx \frac{\textbf{u}^{k+1} - \textbf{u}^k}{\Delta t}
\end{align}
replaces the time derivation by a time difference. Therefore, a time-stepping approach is used. The step size $\Delta t = t^{k+1}- t^k$ is the difference between two time steps $t^k$. Moreover, the value $u^k$ represents the solution of the problem in the last time step $t^k$. For the solution in the first time step $t^1$ the former solution $u^k = T_{0}$ represents the  initial thermal condition of the field problem.\\

With the applied implicit Euler method, the ordinary differential equation \eqref{eq:TherFEMDiff} results in 
\begin{align}
A \textbf{u}^{k+1} +M\frac{1}{\Delta t}( \textbf{u}^{k+1}- \textbf{u}^k) &= \textbf{f}^{k+1}
\end{align}
or further in 
\begin{align}
\Delta tA \textbf{u}^{k+1} +M( \textbf{u}^{k+1}- \textbf{u}^k) &= \Delta t\textbf{f}^{k+1}.
\end{align}
Moving all known terms to the right hand side of the equation yields
\begin{align}
\textbf{u}^{k+1} &= \underbrace{(\Delta tA +M)^{-1}}(\Delta t\textbf{f} + M) \textbf{u}^k.\\
&\qquad=(M^*)^{-1}
\end{align}
as final equation to be solved in each time step. 

\subsection{Solving the Stationary Equation}
In the stationary case, all transient processes have subsided. Hence, the second term in \eqref{eq:TherFEMDiff}, which holds the time-derivation, is zero. \\
Therefore, the stationary heat equation 
\begin{align}
\nabla\cdot(\lambda\nabla T) = - p\quad\textnormal{in } \Omega
\end{align}
is derived. The boundary values \eqref{eq:TherBV1} and \eqref{eq:TherBV2} still apply.  Further, the weak formulation 
\begin{align}
\int_\Omega\lambda\nabla T\cdot\nabla v \;d\Omega = \int_\Omega p\;v\;d\Omega
\end{align}
of the coupled problem is formed analogously to the weak formulation in \cref{sub:ThermalWeak}. Moreover, the weak formulation of the non-linear coupled field problem
\begin{subequations}
	\label{eq:BVPCoupledFPstationary}
	\begin{align}
	\begin{split}
	\int_\Omega\lambda_{FP}\nabla T\cdot\nabla v_T \;d\Omega  =\;& \int_\Omega\Big(\lambda_{FP} - \lambda(T)\Big)\nabla T^{k}\cdot\nabla v_T \;d\Omega \\&+ \int_\Omega p(T)\;v_T\;d\Omega	
	\end{split}
	&&\textnormal{in } \Omega	\\
	\int_{\Omega_{CU}}\sigma_{FP}\nabla\Phi\cdot\nabla v_E\;d\Omega =& \int_{\Omega_{CU}}\Big(\sigma_{FP} - \sigma(T)\Big)\nabla\Phi^k\nabla v_E && \textnormal{in } \Omega_{CU}\\
	T =&\; T_0 &&\textnormal{on }\Gamma_{D}\\
	\Phi =&\; \frac{U_0}{2} &&\textnormal{on }\Gamma_{D,{left}}\\
	\Phi =&\; -\frac{U_0}{2} &&\textnormal{on }\Gamma_{D,{right}}\\
	-\textbf{n}\cdot(\lambda\nabla T) = &\; 0 &&\textnormal{on }\Gamma_{T, N}\\
	-\textbf{n}\cdot\sigma\nabla\Phi = &\; 0 &&\textnormal{on }\Gamma_{E,N}. 
	\end{align}
\end{subequations}
is derived.
 After assembling the stationary \gls{FEM}-matrix and the load vector, 
the equation system 
\begin{align}
A\textbf{u} = \textbf{f}
\end{align}
has to be solved. The vector \textbf{u} represents the approximative  solution compounded by the scalar electric potential $\Phi$ and the temperature $T$ in the stationary case. 
\cref{fig:ThTstaionary} shows a plot of the simulated temperature in the stationary case for the second geometry.
\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\linewidth]{Tstationary3U0225mV}
	\caption{Plot of the simulated temperature $T$ in the stationary case in the $y=0$ plane. The voltage is set to $U_0 = 225$ mV and the third geometry is used.}
	\label{fig:ThTstaionary}
\end{figure}


