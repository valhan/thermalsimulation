# import netgen.gui
from myPackage import cmdInput, loadView
import importlib as  il 
from ngsolve import *

import numpy as np
import nonLinCLib as nL


# import netgen.gui
import matplotlib.pyplot  as plt

import sys
import os


SetNumThreads(10)

a = il.import_module("geometry")
tp = il.import_module("temperaturePlot")
# -----------------------------------------------------------------------------
# parameter
# -----------------------------------------------------------------------------
order = 2       # order of Finite Element Space
maxh = 10        # in mm 
outer=[10, 10 , 10] # far boundary

if len(sys.argv) == 1:
    print("arguments: Geometry in{1, 2, 3}; U0 in V; \n\tdefault: 1 0.125")
    input("press Enter")


if len(sys.argv) > 1:
    geometry = int(sys.argv[1]) #[1,2,3]
else:
    geometry = 1



if len(sys.argv) > 2:
    U0 = float(sys.argv[2])
else:
    U0 = 0.125

limit_for_open = 1 #% of the narrow track


# -----------------------------------------------------------------------------
# material parameter
# -----------------------------------------------------------------------------
limit_for_open *= 1/100
#cu
rhoCU = 8.92e3
cCU = 385
EMeltCU = 13.05e3
mCU=63.54e-3

sigmaCU_T20 = 58e6
sigmaCU_alpha = 3.93e-3 # with sigma = sigmaCU_T20/(1 + alpha(T - T20))

T_KL = [0, 50, 100, 300, 406, 700, 715, 1083] # °C
lambdaCU_KL = [401, 394, 393, 368, 356, 320, 317, 264] # W/mK

T_melt_CU = 1084.62

# FR4
rhoFR4 = 2e3
cFR4=950
lambdaFR4xy = 0.8
lambdaFR4z = 0.55

# air
lambdaAir = 0.0262
rhoAir = 1.1839
cAir = 1005 

# initial temp 
T_init = 25 #°C


if False:
    plt.figure(1)
    plt.clf()
    plt.plot(T_KL, lambdaCU_KL)
    plt.plot(T_KL, lambdaCU_KL, 'o')
    plt.grid()
    plt.xlabel("Temperature in °C")
    plt.ylabel("Thermal Conductivity in W/(m K)")
    plt.title("Thermal Conductivity of Copper")
    plt.show(0)
    cmdInput(locals(), globals())
# -----------------------------------------------------------------------------
# mesh generation
# -----------------------------------------------------------------------------
print("------load geometry-------")
#name = "printedFuse"
scale = 1e-3
copper_height=0.018
if geometry == 1:
    length_straight = 0.05
    if not os.path.isfile("geo1.vol"):
        a.firstGeometry(name="geo1", outer=outer, maxh=maxh, scale = scale, copper_height=copper_height)
    mesh = Mesh("geo1.vol")
elif geometry == 2:
    length_straight = 1.05
    if not os.path.isfile("geo2.vol"):
        a.secondGeometry(name="geo2", outer=outer, maxh=maxh, scale = scale, copper_height=copper_height)
    mesh = Mesh("geo2.vol")
else:
    length_straight = 1.05
    if not os.path.isfile("geo3.vol"):
        a.thirdGeometry(name="geo3", outer=outer, maxh=maxh, scale = scale, copper_height=copper_height)
    mesh = Mesh("geo3.vol")

#a.meanderGeometry(name=name, outer=[10, 10 , 10], length_straight=3, NumMeander=10, meander_copper_pad_width=2, meander_copper_pad_length=0, maxh=1)
#mesh = Mesh(name + ".vol")

Draw(CoefficientFunction([0, 1, 2]), mesh, "mat", draw_surf=False)

bound_val = {"CUleft": 4, "CUright":3, "airleft":9, "airright":6, "airtop":7, "airbottom":1, "airback":2, "airfront":5 }
bound = CoefficientFunction([ bound_val[bc] if bc in bound_val.keys() else 0 for bc in mesh.GetBoundaries() ])
#print ("mu_coef=", mu_coef)
Draw(bound,mesh,'boundaries', draw_surf=True )


maskCU = CoefficientFunction([0, 0, 1])
Draw(maskCU, mesh, "maskCU")


#mip = mesh(0, 0, 0.18/2 * scale)
# ++++++++++++++++++++++
# VSpace electric
# ++++++++++++++++++++++
fesE = H1(mesh, order = order, dirichlet="CUleft|CUright", definedon=mesh.Materials("CU"))

# ++++++++++++++++++++++
# VSpace thermal
# ++++++++++++++++++++++
fesT = H1(mesh, order = order, dirichlet="CUleft|CUright")
VSpace = FESpace([fesE, fesT])

sol = GridFunction(VSpace, "sol")
sol_old = GridFunction(VSpace, "sol_old")
sol_it_old = GridFunction(VSpace, "sol_it_old")
sol_BND = GridFunction(VSpace, "sol_BND")


phi = sol.components[0]
phi_old = sol_old.components[0]
phi_it_old = sol_it_old.components[0]

TR = sol.components[1]
TR_old = sol_old.components[1]
TR_it_old = sol_it_old.components[1]

uE, uT = VSpace.TrialFunction()
vE, vT = VSpace.TestFunction()


T_full = TR + T_init

melting = IfPos(T_full - T_melt_CU, 1, 0)
T = IfPos(melting, T_melt_CU, T_full)
T_melting = IfPos(melting, T_full - T_melt_CU , 0)


q = -grad(TR)

res = sol.vec.CreateVector()

Draw(T, mesh, "T")
Draw(q, mesh, "q")


# ++++++++++++++++++++++
# parameters Energy
# ++++++++++++++++++++++

Energy_melting = cCU * rhoCU * (T_melting) * maskCU
Energy_melted = EMeltCU/mCU * rhoCU

mask_fuse = IfPos(x + length_straight/2*scale, 1, 0) * IfPos(x - length_straight/2*scale, 0, 1)*maskCU

melted = IfPos((Energy_melting - Energy_melted)*mask_fuse, 1, 0) * 1/Integrate(mask_fuse, mesh)

Draw(melted, mesh, "melted")
Draw(melting, mesh, "melting")
# ++++++++++++++++++++++
# parameters electric
# ++++++++++++++++++++++
intrule = IntegrationRule(TET, order*2)

T_KL_CU = np.linspace(0, 1000, 100)
sigma_KL_CU = sigmaCU_T20/(1 + sigmaCU_alpha*(T_KL_CU - 20))

if False:
    plt.figure(2)
    plt.clf()
    plt.plot(T_KL_CU, sigma_KL_CU)
    plt.grid()
    plt.xlabel("Temperature in °C")
    plt.ylabel("Electrical Conductivity in 1/(m Ohm)")
    plt.title("Electrical Conductivity of Copper")
    plt.show(0)
    cmdInput(locals(), globals())
sigmaCU = nL.NonLinCF(mesh, intrule, T, T_KL_CU, sigma_KL_CU, mask=maskCU)



sigmaCU_FP = 1/2*(np.min(sigma_KL_CU) + np.max(sigma_KL_CU))


E = -grad(phi)
J = sigmaCU * E

# calc losses
p_elec =  InnerProduct(E, J)

Draw(E, mesh, "E")
Draw(J, mesh, "J")
Draw(phi, mesh, "phi")
Draw(p_elec, mesh, "Losses")
Draw(CoefficientFunction([0, 0, sigmaCU]), mesh, "sigmaCU", draw_surface=False)

# ++++++++++++++++++++++
# parameters thermal
# ++++++++++++++++++++++
lambdaFR4 = CoefficientFunction((lambdaFR4xy, 0, 0, 0, lambdaFR4xy, 0, 0, 0, lambdaFR4z))
lambdaFR4.dims = (3, 3)

lambdaCU = nL.NonLinCF(mesh, intrule, T, T_KL, lambdaCU_KL, mask=maskCU)


c = CoefficientFunction([cAir, cFR4, cCU])
rho = CoefficientFunction([rhoAir, rhoFR4, rhoCU])

dmin = np.min(lambdaCU_KL)
dmax = np.max(lambdaCU_KL)
lambdaFP = (dmax+dmin)/2


Draw(CoefficientFunction([0, 0, lambdaCU]), mesh, "lambdaCU", draw_surface=False)


with TaskManager():

       # ++++++++++++++++++++++
    # BFI and LFI electric
    # ++++++++++++++++++++++
    a = BilinearForm(VSpace, symmetric=True)
    a += SymbolicBFI(sigmaCU_FP*grad(uE)*grad(vE))
    
    
    

    # ++++++++++++++++++++++
    # BFI and LFI thermal
    # ++++++++++++++++++++++
    a += SymbolicBFI((lambdaFR4*grad(uT))*grad(vT), definedon=mesh.Materials("FR4"))
    a += SymbolicBFI(lambdaAir*grad(uT)*grad(vT), definedon=mesh.Materials("air"))
    a += SymbolicBFI(lambdaFP*grad(uT)*grad(vT), definedon=mesh.Materials("CU"))
    a.Assemble()



    # ++++++++++++++++++++++
    # solve
    # ++++++++++++++++++++++
    # Au = f
    # u = (A)\(f)


    mstar = a.mat.CreateMatrix()
    mstar.AsVector().data = a.mat.AsVector()  

    invmstar = mstar.Inverse(freedofs=VSpace.FreeDofs(), inverse="sparsecholesky")
    
    # right hand side 
    f = LinearForm(VSpace)
    f += SymbolicLFI((sigmaCU_FP - sigmaCU)*phi_old * vE, definedon=mesh.Materials("CU"))
    f += SymbolicLFI(p_elec * vT, definedon=mesh.Materials("CU"))
    f += SymbolicLFI((lambdaFP - lambdaCU)*TR_old * vT, definedon=mesh.Materials("CU"))



# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# ------------------------------------------------------------------------------
# -----------   solve stationary field problem
# ------------------------------------------------------------------------------
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++





loadView()


xi = np.linspace(-4.5, 4.5, 5000) * scale
T_ti = np.zeros([1, len(xi)])


print("-------------------Start stationary simulation------------------")

T_melted_sim = -1
I_max = -1
with TaskManager():


    sol_old.vec.data = sol.vec
    #TR.Set(0, definedon=mesh.Boundaries("CUleft|CUright"))
    val = {"CUleft":U0/2, "CUright":-U0/2}
    bnd_val = CoefficientFunction([val[key] if key in val.keys() else 0 for key in mesh.GetBoundaries()])
    sol_BND.components[0].Set(bnd_val, definedon=mesh.Boundaries("CUleft|CUright"))
    sol_BND.components[1].Set(0, definedon=mesh.Boundaries("CUleft|CUright"))
    

    

    sigmaCU.Update()
    lambdaCU.Update()


    it = 0
    while True:

        it += 1
        print ("Inner Iteration",it)
        
        sol_it_old.vec.data = sol.vec

        
        # solve electric problem
        f.Assemble()
        sol.components[0].vec.data = sol_BND.components[0].vec
        sol.components[1].vec.data = sol_BND.components[1].vec
        # solve thermal problem
        res.data = f.vec - mstar*sol_BND.vec
        sol.vec.data += invmstar * res
        

        sigmaCU.Update()
        lambdaCU.Update()


        errL2T=Integrate(InnerProduct((TR - TR_it_old), (TR - TR_it_old))				
                            ,mesh, definedon=mesh.Materials("CU"))
                            
        solL2T=Integrate(InnerProduct(TR, TR),mesh, definedon=mesh.Materials("CU"))


        errL2E=Integrate(InnerProduct((phi - phi_it_old), (phi - phi_it_old))				
                            ,mesh, definedon=mesh.Materials("CU"))
                            
        solL2E=Integrate(InnerProduct(phi, phi),mesh, definedon=mesh.Materials("CU"))

        # if solL2 > 0:
        #     print("err:",errL2/solL2)
        #     print("solL2:",solL2)
        #     print("errL2:",errL2)
        if it == 1:
            continue

        if solL2E == 0 or solL2T ==0:
            break
        if errL2T/solL2T + errL2E/solL2E < 1e-5:
            # print("Iterations:",it)
            break
            
            
        if it==50:
            print("warning: too many iterations", it)
            wentWrong=True
            # print("managed accuracy",errL2/solL2)
            break
    

    T_ti[0, :] = T(mesh(xi, 0, copper_height/2*scale)).transpose()[0]
    
    Redraw()
    tp.temperaturePlot(xi, T_ti, [-1], height = 20)
    has_melted = Integrate(melted, mesh) > limit_for_open
    if(has_melted):
        print("********************Copper has melted*********************")
        T_melted_sim = T(mesh(0, 0, copper_height/2*scale))
        P = 0
        I_max = 0
    else:
        P = Integrate(p_elec, mesh)
        I_max = P/U0            
        R = U0**2/P




# -------------------- post processing ----------------



# --- calc current ----
# eps = 0.5*scale
# I_vol = IfPos(x + (4.5*scale), 1, 0) * IfPos(x + (4.5*scale) - eps, 0, 1) * mask
# I = Integrate(J[0]*I_vol, mesh)/Integrate(I_vol, mesh)* (copper_height*scale * 4*scale)


print("------------------- finished stationary simulation------------------")


print("used geometry:\t", geometry)
print("U0:\t", U0)




print("Ohm Losses:\t", P)
if has_melted:
    print("has melted")
    print("maximum current:\t", I_max)
else:
    print("has NOT melted")

print("how much has melted?\t", Integrate(melted, mesh)*100, "%")

print(geometry, "&", np.round(U0, 2), "&",np.round(I_max, 2), "&",np.round(P,2), "&",has_melted ,"(",np.round(Integrate(melted, mesh)*100,2), "\%)\\\\\hline" )

# import pickle as pl
# pl.dump({"Ti":T_ti, "xi":xi, "I":I, "P":P, "R":R, "melted":has_melted, "U0": U0}, open("./save/stat/Ti_stat_"+str(geometry) + "_"+str(U0)+".pkl", "wb"))

cmdInput(locals(), globals())
