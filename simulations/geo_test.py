
from netgen.csg import *

def foo(scale = 1):
    thickness = 0.18


    origin = Pnt(0, 0, 0)
    corner = Pnt(9*scale, 4*scale, thickness*scale)
    left  = Plane (origin, Vec(-1,0,0) )
    right = Plane (corner, Vec( 1,0,0) )
    front = Plane (origin, Vec(0,-1,0) )
    back  = Plane (corner, Vec(0, 1,0) )
    bot   = Plane (origin, Vec(0,0,-1) )
    top   = Plane (corner, Vec(0,0, 1) )

    cube = left * right * front * back * bot * top
    geo = CSGeometry()
    geo.Add (cube)
    geo.SetBoundingBox(Pnt(-10*scale, -10*scale, -10*scale), Pnt(10*scale, 10*scale, 10*scale))
    mesh = geo.GenerateMesh(maxh=1)
    mesh.Save("cube.vol")


if __name__ == "__main__":
    from ngsolve import *
    ngsglobals.msg_level = 0
    print("it works with scale 1:")
    foo(1)
    print("it works with scale 0.1:")
    foo(0.1)
    print("it works with scale 0.01:")
    foo(0.01)
    print("it does  not work with scale 0.001:")
    foo(0.001)
    

    # from ngsolve import *
    # mesh = Mesh("cube.vol")

    # import netgen.gui
    # Draw(mesh)
    print("done")
    # input()
