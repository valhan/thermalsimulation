# import netgen.gui
from myPackage import cmdInput, loadView
import importlib as  il 
from ngsolve import *

import numpy as np


import netgen.gui
import matplotlib.pyplot  as plt


a = il.import_module("geometry")
# -----------------------------------------------------------------------------
# parameter
# -----------------------------------------------------------------------------
order = 2       # order of Finite Element Space
maxh = 10        # in mm 
outer=[10, 10 , 10] # far boundary


geometry = 3 #[1,2,3]


t_end = 3
N_t = 100

U0 = 25

# -----------------------------------------------------------------------------
# material parameter
# -----------------------------------------------------------------------------

#cu
rhoCU = 8.92e3
cCU = 385
EMeltCU = 13.05e3
mCU=63.54e-3

sigmaCU = 58e6

T_KL = [0, 50, 100, 300, 406, 700, 715, 1083] # °C
lambdaCU_KL = [401, 394, 393, 368, 356, 320, 317, 264] # W/mK
# lambdaCU_KL = [322.5, 322.5, 322.5, 322.5, 322.5, 322.5, 322.5, 322.5] # W/mK
# plt.figure(1)
# plt.plot(T_KL, lambdaCU_KL)
# plt.grid()
# plt.show(0)

# FR4
rhoFR4 = 2e3
cFR4=950
lambdaFR4xy = 0.8
lambdaFR4z = 0.55

# air
lambdaAir = 0.0262
rhoAir = 1.1839
cAir = 1005 

# initial temp 
T_init = 25 #°C
# -----------------------------------------------------------------------------
# mesh generation
# -----------------------------------------------------------------------------
print("------load geometry-------")
name = "printedFuse"
scale = 1e-3
if geometry == 1:
    a.firstGeometry(name=name, outer=outer, maxh=maxh, scale = scale)
elif geometry == 2:
    a.secondGeometry(name=name, outer=outer, maxh=maxh, scale = scale)
else:
    a.thirdGeometry(name=name, outer=outer, maxh=maxh, scale = scale)

#a.meanderGeometry(name=name, outer=[10, 10 , 10], length_straight=3, NumMeander=10, meander_copper_pad_width=2, meander_copper_pad_length=0, maxh=1)
mesh = Mesh(name + ".vol")

Draw(CoefficientFunction([0, 1, 2]), mesh, "mat", draw_surf=False)

bound_val = {"CUleft": 4, "CUright":3, "airleft":9, "airright":6, "airtop":7, "airbottom":1, "airback":2, "airfront":5 }
bound = CoefficientFunction([ bound_val[bc] if bc in bound_val.keys() else 0 for bc in mesh.GetBoundaries() ])
#print ("mu_coef=", mu_coef)
Draw(bound,mesh,'boundaries', draw_surf=True )


mask = CoefficientFunction([0, 0, 1])
Draw(mask, mesh, "maskCU")



# ------------------------------------------------------------------------------
# -----------   solve electirc field
# ------------------------------------------------------------------------------


# ++++++++++++++++++++++
# VSpace
# ++++++++++++++++++++++
fesE = H1(mesh, order = order, dirichlet="CUleft|CUright", definedon=mesh.Materials("CU"))

uE = fesE.TrialFunction()
vE = fesE.TestFunction()

phi = GridFunction(fesE, "phi")

E = -grad(phi)
J = sigmaCU * E


# calc losses
p_elec =  InnerProduct(E, J)

Draw(E, mesh, "E")
Draw(J, mesh, "J")
Draw(phi, mesh, "phi")
Draw(p_elec, mesh, "Losses")
# ++++++++++++++++++++++
# Boundary values
# ++++++++++++++++++++++
#Dirichlet boundary condition
val = {"CUleft":-U0*x, "CUright":-U0*x}
bnd_val = CoefficientFunction([val[key] if key in val.keys() else 0 for key in mesh.GetBoundaries()])
phi.Set(bnd_val, definedon=mesh.Boundaries("CUleft|CUright"))


# ++++++++++++++++++++++
# BFI and LFI
# ++++++++++++++++++++++
aE = BilinearForm(fesE, symmetric=True)
aE += SymbolicBFI(sigmaCU*grad(uE)*grad(vE))
cE = Preconditioner(aE, type = "direct")

# right hand side 
fE = LinearForm(fesE)
 
aE.Assemble()
fE.Assemble()


# ++++++++++++++++++++++
# solve
# ++++++++++++++++++++++
BVP(bf = aE, lf = fE, gf = phi, pre = cE, maxsteps=50).Do()

print("---------------finished electric field simulation--------------")

# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# ------------------------------------------------------------------------------
# -----------   solve thermal field
# ------------------------------------------------------------------------------
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TK0 = 273.15
TR0 = TK0 + T_init
TR_init = 0

lambdaCU_KL = np.array(lambdaCU_KL)
T_KL = np.array(T_KL) 

ti = np.linspace(0, t_end, N_t)
dt = t_end/(N_t-1)
# ++++++++++++++++++++++
# VSpace
# ++++++++++++++++++++++
fesT = H1(mesh, order = order, dirichlet="CUleft|CUright")
TR = GridFunction(fesT, "TR")
TR_old = GridFunction(fesT, "TR_old")
TR_it_old = GridFunction(fesT, "TR_it_old")

uT = fesT.TrialFunction()
vT = fesT.TestFunction()

T = TR + T_init

q = -grad(TR)

res = TR.vec.CreateVector()

Draw(T, mesh, "T")
Draw(q, mesh, "q")
# ++++++++++++++++++++++
# parameters Energy
# ++++++++++++++++++++++

Energy = cCU * rhoCU * (T - T_init) * mask
Energy_melt = EMeltCU/mCU * rhoCU

melting = IfPos(Energy-Energy_melt, 1, 0) * mask
Draw(CoefficientFunction([0, 0, Energy/Energy_melt]), mesh, "Energy")
Draw(melting, mesh, "melting")

# ++++++++++++++++++++++ 
# parameter
# ++++++++++++++++++++++

# lambdaFR4 = CoefficientFunction((lambdaFR4xy, 0, 0, 0, lambdaFR4xy, 0, 0, 0, lambdaFR4z))
# lambdaFR4.dims = (3, 3)

lambdaFR4 = CoefficientFunction((lambdaFR4xy, 0, 0, 0, lambdaFR4xy, 0, 0, 0, lambdaFR4z))
lambdaFR4.dims = (3, 3)

#Tlambda_KL = BSpline(2, [0] + T_KL, lambdaCU_KL)
import nonLinCLib as nL
intrule = IntegrationRule(TET, order*2)
lambdaCU = nL.NonLinCF(mesh, intrule, T, T_KL, lambdaCU_KL, mask=mask)


c = CoefficientFunction([cAir, cFR4, cCU])
rho = CoefficientFunction([rhoAir, rhoFR4, rhoCU])


dmin = np.min(lambdaCU_KL)
dmax = np.max(lambdaCU_KL)
lambdaFP = (dmax+dmin)/2

# ++++++++++++++++++++++
# BFI and LFI
# ++++++++++++++++++++++
aT = BilinearForm(fesT, symmetric=True)
aT += SymbolicBFI((lambdaFR4*grad(uT))*grad(vT), definedon=mesh.Materials("FR4"))
aT += SymbolicBFI(lambdaAir*grad(uT)*grad(vT), definedon=mesh.Materials("air"))
aT += SymbolicBFI(lambdaFP*grad(uT)*grad(vT), definedon=mesh.Materials("CU"))
aT.Assemble()

mT = BilinearForm(fesT, symmetric=True)
mT += SymbolicBFI(rho*c*uT*vT)
mT.Assemble()


# right hand side 
fT = LinearForm(fesT)
fT += SymbolicLFI(p_elec * vT, definedon=mesh.Materials("CU"))
fT += SymbolicLFI((lambdaFP - lambdaCU)*TR_old * vT, definedon=mesh.Materials("CU"))
# ++++++++++++++++++++++
# solve
# ++++++++++++++++++++++
# Au + d/dt Mu = f
# Au + M(u - u_old)/dt = f
# dtAu + Mu - Mu_old = dtf
# dtAu + Mu = dtf + Mu_old
# u = (dtA + M)\(dtf + Mu_old)


mstar = mT.mat.CreateMatrix()
mstar.AsVector().data = dt * aT.mat.AsVector() + mT.mat.AsVector() 
invmstar = mstar.Inverse(freedofs=fesT.FreeDofs())

loadView()

cmdInput(locals(), globals())
print("-------------------Start thermal simulation------------------")
mip = mesh(0, 0, 0.018e-3)
with TaskManager():

    for i in range(N_t):
        print("########### step "  + str(i) +" / " + str(N_t) +" ti:\t" + str(ti[i])+ "###########")
        TR_old.vec.data = TR.vec
        TR.Set(0, definedon=mesh.Boundaries("CUleft|CUright"))


        it = 0
        while True:

            it += 1
            print ("Inner Iteration",it)
            TR_it_old.vec.data = TR.vec

            fT.Assemble()

            # invert matrix
            res.data = dt*fT.vec + mT.mat*TR_old.vec
            TR.vec.data = invmstar * res


            lambdaCU.Update()


            errL2=Integrate(InnerProduct((TR - TR_it_old), (TR - TR_it_old))				
                                ,mesh, definedon=mesh.Materials("CU"))
                                
            solL2=Integrate(InnerProduct(TR, TR),mesh, definedon=mesh.Materials("CU"))
    
            # if solL2 > 0:
            #     print("err:",errL2/solL2)
            #     print("solL2:",solL2)
            #     print("errL2:",errL2)


            if solL2==0:
                break
            if errL2/solL2 < 1e-5:
                # print("Iterations:",it)
                break
                
                
            if it==10:
                print("warning: too many iterations", it)
                wentWrong=True
                # print("managed accuracy",errL2/solL2)
                break
        
        Redraw()



xi = np.linspace(-outer[0]/2, outer[0]/2, 100) * scale
Ti = T(mesh(xi, 0, 0))

plt.figure(1)
plt.plot(xi*1000, Ti)
plt.xlabel("x in mm")
plt.ylabel("T in °C")
plt.grid()
plt.show(0)

print("------------------- finished thermal simulation------------------")


print("used geometry:\t", geometry)
print("U0:\t", U0)
print("Ohm Losses:\t", Integrate(p_elec, mesh))



cmdInput(locals(), globals())
