# import netgen.gui
from myPackage import cmdInput, loadView
import importlib as  il 
from ngsolve import *

import numpy as np
import nonLinCLib as nL


import netgen.gui
import matplotlib.pyplot  as plt

import sys
import os


a = il.import_module("geometry")
tp = il.import_module("temperaturePlot")
# -----------------------------------------------------------------------------
# parameter
# -----------------------------------------------------------------------------
order = 2       # order of Finite Element Space
maxh = 10        # in mm 
outer=[10, 10 , 10] # far boundary


if len(sys.argv) > 1:
    geometry = int(sys.argv[1]) #[1,2,3]
else:
    geometry = 1



if len(sys.argv) > 2:
    U0 = float(sys.argv[2])
else:
    U0 = 0.125

if len(sys.argv) > 3:
    t_end = float(sys.argv[3])
else:
    t_end = 3



# -----------------------------------------------------------------------------
# material parameter
# -----------------------------------------------------------------------------

#cu
rhoCU = 8.92e3
cCU = 385
EMeltCU = 13.05e3
mCU=63.54e-3

sigmaCU_T20 = 58e6
sigmaCU_alpha = 3.93e-3 # with sigma = sigmaCU_T20/(1 + alpha(T - T20))

T_KL = [0, 50, 100, 300, 406, 700, 715, 1083] # °C
lambdaCU_KL = [401, 394, 393, 368, 356, 320, 317, 264] # W/mK

T_melt_CU = 1084.62

# FR4
rhoFR4 = 2e3
cFR4=950
lambdaFR4xy = 0.8
lambdaFR4z = 0.55

# air
lambdaAir = 0.0262
rhoAir = 1.1839
cAir = 1005 

# initial temp 
T_init = 25 #°C
# -----------------------------------------------------------------------------
# mesh generation
# -----------------------------------------------------------------------------
print("------load geometry-------")
#name = "printedFuse"
scale = 1e-3
copper_height=0.18
if geometry == 1:
    if not os.path.isfile("geo1.vol"):
        a.firstGeometry(name="geo1", outer=outer, maxh=maxh, scale = scale, copper_height=copper_height)
    mesh = Mesh("geo1.vol")
elif geometry == 2:
    if not os.path.isfile("geo2.vol"):
        a.secondGeometry(name="geo2", outer=outer, maxh=maxh, scale = scale, copper_height=copper_height)
    mesh = Mesh("geo2.vol")
else:
    if not os.path.isfile("geo3.vol"):
        a.thirdGeometry(name="geo3", outer=outer, maxh=maxh, scale = scale, copper_height=copper_height)
    mesh = Mesh("geo3.vol")

#a.meanderGeometry(name=name, outer=[10, 10 , 10], length_straight=3, NumMeander=10, meander_copper_pad_width=2, meander_copper_pad_length=0, maxh=1)
#mesh = Mesh(name + ".vol")

Draw(CoefficientFunction([0, 1, 2]), mesh, "mat", draw_surf=False)

bound_val = {"CUleft": 4, "CUright":3, "airleft":9, "airright":6, "airtop":7, "airbottom":1, "airback":2, "airfront":5 }
bound = CoefficientFunction([ bound_val[bc] if bc in bound_val.keys() else 0 for bc in mesh.GetBoundaries() ])
#print ("mu_coef=", mu_coef)
Draw(bound,mesh,'boundaries', draw_surf=True )


mask = CoefficientFunction([0, 0, 1])
Draw(mask, mesh, "maskCU")


#mip = mesh(0, 0, 0.18/2 * scale)
# ++++++++++++++++++++++
# VSpace electric
# ++++++++++++++++++++++
fesE = H1(mesh, order = order, dirichlet="CUleft|CUright", definedon=mesh.Materials("CU"))

uE = fesE.TrialFunction()
vE = fesE.TestFunction()

phi = GridFunction(fesE, "phi")
phi_old = GridFunction(fesE, "phi_old")
phi_it_old = GridFunction(fesE, "phi_it_old")






# ++++++++++++++++++++++
# VSpace thermal
# ++++++++++++++++++++++
fesT = H1(mesh, order = order, dirichlet="CUleft|CUright")
TR = GridFunction(fesT, "TR")
TR_old = GridFunction(fesT, "TR_old")
TR_it_old = GridFunction(fesT, "TR_it_old")

uT = fesT.TrialFunction()
vT = fesT.TestFunction()


T_full = TR + T_init

melting = IfPos(T_full - T_melt_CU, 1, 0) * mask
T = IfPos(melting, T_melt_CU, T_full)
T_melting = IfPos(melting, T_full - T_melt_CU , 0)


q = -grad(TR)

res = TR.vec.CreateVector()

Draw(T, mesh, "T")
Draw(q, mesh, "q")


# ++++++++++++++++++++++
# parameters Energy
# ++++++++++++++++++++++

Energy_melting = cCU * rhoCU * (T_melting) * mask
Energy_melted = EMeltCU/mCU * rhoCU

melted = IfPos(Energy_melting - Energy_melted, 1, 0)

Draw(melted, mesh, "melted")
Draw(melting, mesh, "melting")
# ++++++++++++++++++++++
# parameters electric
# ++++++++++++++++++++++
intrule = IntegrationRule(TET, order*2)

T_KL_CU = np.linspace(0, 1000, 100)
sigma_KL_CU = sigmaCU_T20/(1 + sigmaCU_alpha*(T_KL_CU - 20))
sigmaCU = nL.NonLinCF(mesh, intrule, T, T_KL_CU, sigma_KL_CU, mask=mask)



sigmaCU_FP = 1/2*(np.min(sigma_KL_CU) + np.max(sigma_KL_CU))


E = -grad(phi)
J = sigmaCU * E

# calc losses
p_elec =  InnerProduct(E, J)

Draw(E, mesh, "E")
Draw(J, mesh, "J")
Draw(phi, mesh, "phi")
Draw(p_elec, mesh, "Losses")
Draw(CoefficientFunction([0, 0, sigmaCU]), mesh, "sigmaCU", draw_surface=False)

# ++++++++++++++++++++++
# parameters thermal
# ++++++++++++++++++++++
lambdaFR4 = CoefficientFunction((lambdaFR4xy, 0, 0, 0, lambdaFR4xy, 0, 0, 0, lambdaFR4z))
lambdaFR4.dims = (3, 3)

lambdaCU = nL.NonLinCF(mesh, intrule, T, T_KL, lambdaCU_KL, mask=mask)


c = CoefficientFunction([cAir, cFR4, cCU])
rho = CoefficientFunction([rhoAir, rhoFR4, rhoCU])

dmin = np.min(lambdaCU_KL)
dmax = np.max(lambdaCU_KL)
lambdaFP = (dmax+dmin)/2


Draw(CoefficientFunction([0, 0, lambdaCU]), mesh, "lambdaCU", draw_surface=False)


with TaskManager():

    # ++++++++++++++++++++++
    # BFI and LFI electric
    # ++++++++++++++++++++++
    aE = BilinearForm(fesE, symmetric=True)
    aE += SymbolicBFI(sigmaCU_FP*grad(uE)*grad(vE))
    cE = Preconditioner(aE, type = "direct")
    aE.Assemble()
    # right hand side 
    fE = LinearForm(fesE)
    fE += SymbolicLFI((sigmaCU_FP - sigmaCU)*phi_old * vE)


    # ++++++++++++++++++++++
    # BFI and LFI thermal
    # ++++++++++++++++++++++
    aT = BilinearForm(fesT, symmetric=True)
    aT += SymbolicBFI((lambdaFR4*grad(uT))*grad(vT), definedon=mesh.Materials("FR4"))
    aT += SymbolicBFI(lambdaAir*grad(uT)*grad(vT), definedon=mesh.Materials("air"))
    aT += SymbolicBFI(lambdaFP*grad(uT)*grad(vT), definedon=mesh.Materials("CU"))
    aT.Assemble()


    # right hand side 
    fT = LinearForm(fesT)
    fT += SymbolicLFI(p_elec * vT, definedon=mesh.Materials("CU"))
    fT += SymbolicLFI((lambdaFP - lambdaCU)*TR_old * vT, definedon=mesh.Materials("CU"))
    mstar = aT.mat.CreateMatrix()
    mstar.AsVector().data = aT.mat.AsVector() 
    invmstar = mstar.Inverse(freedofs=fesT.FreeDofs())





# ++++++++++++++++++++++
# solve
# ++++++++++++++++++++++


# ++++++++++++++++++++++
# Boundary values electric
# ++++++++++++++++++++++
#Dirichlet boundary condition





print("---------------meshing and setting parameters--------------")



# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# ------------------------------------------------------------------------------
# -----------   solve thermal field
# ------------------------------------------------------------------------------
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++





loadView()


xi = np.linspace(-4.5, 4.5, 5000) * scale
T_ti = np.zeros([1, len(xi)])


print("-------------------Start thermal simulation------------------")

time_melted = -1
T_melted_sim = -1
I_melted_sim = -1
with TaskManager():

    TR_old.vec.data = TR.vec
    TR.Set(0, definedon=mesh.Boundaries("CUleft|CUright"))


    phi_old.vec.data = phi.vec
    val = {"CUleft":U0/2, "CUright":-U0/2}
    bnd_val = CoefficientFunction([val[key] if key in val.keys() else 0 for key in mesh.GetBoundaries()])
    phi.Set(bnd_val, definedon=mesh.Boundaries("CUleft|CUright"))

    sigmaCU.Update()
    lambdaCU.Update()


    it = 0
    while True:

        it += 1
        print ("Inner Iteration",it)
        TR_it_old.vec.data = TR.vec
        phi_it_old.vec.data = phi.vec

        
        # solve electric problem
        fE.Assemble()
        BVP(bf = aE, lf = fE, gf = phi, pre = cE, maxsteps=50).Do()

        fT.Assemble()

        # solve thermal problem
        res.data = fT.vec
        TR.vec.data = invmstar * res

        sigmaCU.Update()
        lambdaCU.Update()


        errL2T=Integrate(InnerProduct((TR - TR_it_old), (TR - TR_it_old))				
                            ,mesh, definedon=mesh.Materials("CU"))
                            
        solL2T=Integrate(InnerProduct(TR, TR),mesh, definedon=mesh.Materials("CU"))


        errL2E=Integrate(InnerProduct((phi - phi_it_old), (phi - phi_it_old))				
                            ,mesh, definedon=mesh.Materials("CU"))
                            
        solL2E=Integrate(InnerProduct(phi, phi),mesh, definedon=mesh.Materials("CU"))

        # if solL2 > 0:
        #     print("err:",errL2/solL2)
        #     print("solL2:",solL2)
        #     print("errL2:",errL2)


        if solL2E == 0 or solL2T ==0:
            break
        if errL2T/solL2T + errL2E/solL2E < 1e-5:
            # print("Iterations:",it)
            break
            
            
        if it==50:
            print("warning: too many iterations", it)
            wentWrong=True
            # print("managed accuracy",errL2/solL2)
            break
    



    T_ti[0, :] = T(mesh(xi, 0, copper_height/2*scale)).transpose()[0]
    Redraw()
    tp.temperaturePlot(xi, T_ti, [0], height = 20)
    if(Integrate(melted, mesh) > 0):
        print("********************Copper has melted*********************")
        T_melted_sim = T(mesh(0, 0, copper_height/2*scale))

        P = Integrate(p_elec, mesh)
        I_melted_sim = I = P/U0            
        



# -------------------- post processing ----------------




# --- calc current ----
# eps = 0.5*scale
# I_vol = IfPos(x + (4.5*scale), 1, 0) * IfPos(x + (4.5*scale) - eps, 0, 1) * mask
# I = Integrate(J[0]*I_vol, mesh)/Integrate(I_vol, mesh)* (copper_height*scale * 4*scale)

P = Integrate(p_elec, mesh)
R = U0**2/P
I = P/U0
print("------------------- finished thermal simulation------------------")


print("used geometry:\t", geometry)
print("U0:\t", U0)
print("I:\t", I)



print("Ohm Losses:\t", P)
if(time_melted > 0):
    print("time when melted:\t", time_melted)
    print("maximum current:\t", I_melted_sim)
else:
    print("has not melted")


cmdInput(locals(), globals())
