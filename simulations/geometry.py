from ngsolve import *
from netgen.csg import *
from myPackage import cmdInput # for debugging


def firstGeometry(name="tmp", outer=[10, 10 , 10], maxh =1, scale = 1e-3, copper_height=0.018):
    straightGeometry(name=name, outer=outer, length_straight=1, maxh=maxh, scale=scale, copper_height=copper_height)

def secondGeometry(name="tmp", outer=[10, 10 , 10], maxh =1, scale = 1e-3, copper_height=0.018):
    straightGeometry(name=name, outer=outer, length_straight=2, maxh=maxh, scale=scale, copper_height=copper_height)

def thirdGeometry(name="tmp", outer=[10, 10 , 10], maxh =1, scale = 1e-3, copper_height=0.018):
    meanderGeometry(name=name, outer=outer, length_straight=2,NumMeander=5, meander_copper_pad_width=1, meander_copper_pad_length=0, maxh=maxh, scale = scale, copper_height=copper_height)

def straightGeometry(name="tmp", outer=[10, 10 , 10], length_straight=1, maxh=1, scale = 1e-3, copper_height=0.018):
    meanderGeometry(name=name, outer=outer, length_straight=length_straight, NumMeander=0, maxh=maxh, scale=scale, copper_height=copper_height)


def meanderGeometry(name="tmp", outer=[10, 10 , 10], length_straight=1, NumMeander=1, 
    meander_copper_pad_width=1, meander_copper_pad_length=0,  
    copper_height = 0.018, copper_width = 1, copper_width_meander = 0.05, 
    maxh=1, scale = 1e-3
    ):
    maxh *= scale
    length_straight *= scale
    outer = [outer[0]*scale, outer[1]*scale, outer[2]*scale]
    meander_copper_pad_width *= scale
    meander_copper_pad_length *= scale
    copper_width *= scale
    copper_width_meander *= scale
    copper_height *= scale

    # ------------------ air --------------------
    left  = Plane(Pnt(-outer[0]/2,0,0), Vec(-1,0,0) ).bc("airleft")
    right = Plane(Pnt(outer[0]/2,0,0), Vec( 1,0,0) ).bc("airright")
    front = Plane(Pnt(0,-outer[1]/2,0), Vec(0,-1,0) ).bc("airfront")
    back  = Plane(Pnt(0,outer[1]/2, 0), Vec(0, 1,0) ).bc("airleft")
    top_air   = Plane (Pnt(0,0,outer[2]/2), Vec(0,0, 1) ).bc("airtop")

    # ------------------ FR4 --------------------
    # cube = left * right * front * back * bottom * top
    top_FR4 = Plane(Pnt(0, 0, 0), Vec(0, 0, 1))
    bottom_FR4 = Plane(Pnt(0, 0, -1.5* scale), Vec(0, 0, -1))


    # -------------- Copper ----------------
    
    left_copper  = Plane(Pnt(-4.5*scale,0,0), Vec(-1,0,0) ).bc("CUleft")
    right_copper = Plane(Pnt(4.5*scale,0,0), Vec( 1,0,0) ).bc("CUright")
    front_copper = Plane(Pnt(0,-2*scale,0), Vec(0,-1,0) )
    back_copper  = Plane(Pnt(0,2*scale, 0), Vec(0, 1,0) )
    top_copper = Plane (Pnt(0,0,copper_height), Vec(0,0, 1) )
    cube_copper = left_copper * right_copper * front_copper * back_copper * top_copper


    # big cut-out
    cutout_copper_front_ortho_big = OrthoBrick( Pnt(-3.5* scale, -outer[1]/2, -outer[2]/2), Pnt(3.5* scale, -copper_width/2, outer[2]/2))
    cutout_copper_back_ortho_big = OrthoBrick( Pnt(-3.5* scale, copper_width/2, -outer[2]/2), Pnt(3.5* scale, outer[1]/2, outer[2]/2))

    # small cut-out
    left_copper_small = Plane(Pnt(- (length_straight - 1*scale)/2- copper_width_meander/2, 0, 0), Vec(-1, 0, 0))
    right_copper_small = Plane(Pnt((length_straight - 1*scale)/2 + copper_width_meander/2, 0, 0), Vec(1, 0, 0))
    back_copper_small_front = Plane(Pnt(0, -copper_width_meander/2, 0), Vec(0, 1, 0))
    front_copper_small_back = Plane(Pnt(0, copper_width_meander/2, 0), Vec(0, -1, 0))
    cutout_copper_front_ortho_small = left_copper_small*right_copper_small*back_copper_small_front*front*top_air
    cutout_copper_back_ortho_small = left_copper_small*right_copper_small*front_copper_small_back*back*top_air
    # cutout_copper_front_ortho_small = OrthoBrick( Pnt(-0.025, -10, -10), Pnt(0.025, -0.025, 10))
    # cutout_copper_back_ortho_small = OrthoBrick( Pnt(-0.025, 0.025, -10), Pnt(0.025, 10, 10))

    # diagonal cut-out
    pnt_front = Pnt(0, (length_straight-1*scale)/2, 0)    # crossing of right and left plane
    cutout_diag_front_left = Plane(pnt_front, Vec(-1, 1, 0))
    cutout_diag_front_right = Plane(pnt_front, Vec(1, 1, 0))
    cutout_copper_diag_front = front * back_copper_small_front*cutout_diag_front_left*cutout_diag_front_right*top_air

    pnt_back = Pnt(0, -(length_straight-1*scale)/2, 0)    # crossing of right and left plane
    cutout_diag_back_left = Plane(pnt_back, Vec(-1, -1, 0))
    cutout_diag_back_right = Plane(pnt_back, Vec(1, -1, 0))
    cutout_copper_diag_back = front * front_copper_small_back*cutout_diag_back_left*cutout_diag_back_right*top_air

    
    # meander
    if(NumMeander > 1):
        if (meander_copper_pad_length == 0):
            meander_copper_pad_length = (length_straight - 1*scale) * 0.8 

        NumCutout = NumMeander - 1
        gap_length =  (meander_copper_pad_length - (NumMeander * copper_width_meander))/(NumMeander-1)

        # cupper pad
        meander_copper_left = Plane(Pnt(-meander_copper_pad_length/2, 0, 0), Vec(-1, 0, 0))
        meander_copper_right = Plane(Pnt(meander_copper_pad_length/2, 0, 0), Vec(1, 0, 0))

        meander_copper_front = Plane(Pnt(0, -meander_copper_pad_width/2 , 0), Vec(0, -1, 0))
        meander_copper_back = Plane(Pnt(0, meander_copper_pad_width/2 , 0), Vec(0, 1, 0))

        meander_copper_pad = meander_copper_right * meander_copper_left * meander_copper_front * meander_copper_back * top_copper - top_FR4

        #delete first meander
        cut_first = OrthoBrick(Pnt(-meander_copper_pad_length/2*1.1, -outer[1]/2, -outer[2]/2),  Pnt(-meander_copper_pad_length/2 + copper_width_meander+gap_length/2, -copper_width_meander/2, outer[2]/2))
        #delete last meander
        if NumMeander % 2 == 0:
            cut_last = OrthoBrick(Pnt(meander_copper_pad_length/2-copper_width_meander-gap_length/2, -outer[1]/2, -outer[2]/2),  Pnt(meander_copper_pad_length/2*1.1 + copper_width_meander, -copper_width_meander/2, outer[2]/2))
        else:
            cut_last = OrthoBrick(Pnt(meander_copper_pad_length/2-copper_width_meander-gap_length/2, copper_width_meander/2, -outer[2]/2),  Pnt(meander_copper_pad_length/2*1.1 + copper_width_meander, outer[1]/2, outer[2]/2))
        

        x = -meander_copper_pad_length/2+copper_width_meander
        for i in range(NumCutout):
            if i == 0:
                cut_meander = OrthoBrick(Pnt(x, -outer[1]/2, -outer[2]/2),  Pnt(x + gap_length, meander_copper_pad_width/2 - copper_width_meander, outer[2]/2))
                x += copper_width_meander + gap_length
                continue
            if(i % 2) == 0:
                # back cut-out
                cut_meander += OrthoBrick(Pnt(x, -outer[1]/2, -outer[2]/2),  Pnt(x + gap_length, meander_copper_pad_width/2 - copper_width_meander, outer[2]/2))
                x += copper_width_meander + gap_length
            else:
                # front cut-out
                cut_meander += OrthoBrick(Pnt(x, -meander_copper_pad_width/2 + copper_width_meander, -outer[2]/2),  Pnt(x + gap_length, outer[1]/2, outer[2]/2))
                x += copper_width_meander + gap_length

                                
        copper_fullheight = (cube_copper * top_copper
                 - cutout_copper_front_ortho_big - cutout_copper_back_ortho_big
                 - cutout_copper_front_ortho_small - cutout_copper_back_ortho_small
                 - cutout_copper_diag_front - cutout_copper_diag_back
                 + meander_copper_pad - cut_first - cut_last - cut_meander).mat("CU")

    else:
        copper_fullheight = (cube_copper * top_copper 
                - cutout_copper_front_ortho_big - cutout_copper_back_ortho_big
                - cutout_copper_front_ortho_small - cutout_copper_back_ortho_small
                - cutout_copper_diag_front - cutout_copper_diag_back 
                ).mat("CU")

    
    # ------------------- geo --------------------
    FR4 = (right * left * back * front * bottom_FR4 * top_FR4).mat("FR4")
    air_top = ( (right * left * back * front * top_air - copper_fullheight) - top_FR4).mat("air")
    geo = CSGeometry()


    geo.Add(air_top)  
    geo.Add(FR4)
    geo.Add(copper_fullheight - top_FR4)
    geo.SetBoundingBox(Pnt(-10*scale, -10*scale, -10*scale), Pnt(10*scale, 10*scale, 10*scale))
    mesh = geo.GenerateMesh(maxh=maxh)


    mesh.Save(name+".vol")

if __name__ == "__main__":

    from ngsolve import *
    import netgen.gui


    name = "test"
    meanderGeometry(name=name, length_straight=3, NumMeander=10, 
    meander_copper_pad_width=4, meander_copper_pad_length=0,  
    copper_height = 0.018)
    mesh = Mesh(name+".vol")
    Draw(mesh)

    input()

    pass
