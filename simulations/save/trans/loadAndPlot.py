from myPackage import cmdInput

import matplotlib.pyplot as plt

import pickle as pl

import importlib as  il 

import numpy as np








files1 = [  "Ti_trans_1_1.3.pkl" ,
            "Ti_trans_1_1.1.pkl", 
            "Ti_trans_1_0.7.pkl", 
            "Ti_trans_1_0.9.pkl"            
            ]

files2 = [ "Ti_trans_2_1.3.pkl" ,
            "Ti_trans_2_1.1.pkl", 
            "Ti_trans_2_0.9.pkl",
            "Ti_trans_2_0.7.pkl" 
              ]
        
files3 = [ "Ti_trans_3_2.1.pkl" ,
            "Ti_trans_3_1.9.pkl", 
            "Ti_trans_3_1.7.pkl",
            "Ti_trans_3_1.3.pkl",
            "Ti_trans_3_1.1.pkl" 
              ]




ind = [230, 130, 60, 43]
I = [40.55,  28.96, 22.06, 20.13]
for i in range(len(files1)):
    o = pl.load(open(files1[i],"rb"))
    
    T_xx = np.array(o["T_xx"])
    ti = o["ti"]


    U0 = o["U0"]
    I0 = I[i]


    dy = (800-400)/(3)
    plt.figure(1,  figsize=(8,4.5))
    plt.title("Transient Simulation for the First Geometry")
    plt.plot(np.hstack([0, ti]), np.hstack([25, T_xx]))
    plt.annotate("$U_0$ = "+str(U0) + "V, $I_{max}$ = " + str(I0)+ "A", 
                        xy=(ti[ind[i]], T_xx[ind[i]]), xytext=(0.2, 400 + dy*i), 
                        arrowprops=dict(arrowstyle="->"))

    plt.xlabel("Time in s")
    plt.ylabel("Temperature in °C")
    plt.grid(1)
    plt.show(0)



ind = [30, 15, 30, 70]
I = [15.46, 13.35, 9.81, 7.72]
for i in range(len(files2)):
    o = pl.load(open(files2[i],"rb"))
    
    T_xx = np.array(o["T_xx"])
    ti = o["ti"]


    U0 = o["U0"]
    I0 = I[i]


    dy = (800-400)/(3)


    plt.figure(2,  figsize=(8,4.5))
    plt.title("Transient Simulation for the Second Geometry")
    plt.plot(np.hstack([0, ti]), np.hstack([25, T_xx]))
    plt.annotate("$U_0$ = "+str(U0) + "V, $I_{max}$ = " + str(I0)+ "A",
                        xy=(ti[ind[i]], T_xx[ind[i]]), xytext=(0.075, 400 + dy*i), 
                        arrowprops=dict(arrowstyle="->"))

    plt.xlabel("Time in s")
    plt.ylabel("Temperature in °C")
    plt.grid(1)
    plt.show(0)



ind = [135, 75, 35, 75, 135]
I = [5.33, 4.83, 4.31, 3.46, 3.25]
for i in range(len(files3)):
    o = pl.load(open(files3[i],"rb"))
    
    T_xx = np.array(o["T_xx"])
    ti = o["ti"]


    U0 = o["U0"]
    I0 = I[i]


    dy = (800-400)/(4)
    plt.figure(3, figsize=(8,4.5))
    plt.title("Transient Simulation for the Third Geometry")
    plt.plot(np.hstack([0, ti]), np.hstack([25, T_xx]))
    plt.annotate("$U_0$ = "+str(U0) + "V, $I_{max}$ = " + str(I0)+ "A",
                        xy=(ti[ind[i]], T_xx[ind[i]]), xytext=(0.12, 400 + dy*i), 
                        arrowprops=dict(arrowstyle="->"))

    plt.xlabel("Time in s")
    plt.ylabel("Temperature in °C")
    plt.xlim([-0.005, 0.17])
    plt.grid(1)
    plt.show(0)

cmdInput(locals(), globals())