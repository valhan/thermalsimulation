from myPackage import cmdInput

import matplotlib.pyplot as plt

import pickle as pl

import importlib as  il 

import numpy as np


tp = il.import_module("temperaturePlot")





files1 = [   "Ti_stat_1_0.5.pkl", 
            "Ti_stat_1_0.72.pkl", 
            "Ti_stat_1_0.75.pkl", 
            "Ti_stat_1_1.0.pkl"]

files2 = [  "Ti_stat_2_0.5.pkl", 
            "Ti_stat_2_0.72.pkl", 
            "Ti_stat_2_0.75.pkl", 
            "Ti_stat_2_1.0.pkl"]
        
files3 = [  "Ti_stat_3_0.5.pkl", 
            "Ti_stat_3_0.75.pkl", 
            "Ti_stat_3_1.0.pkl", 
            "Ti_stat_3_1.2.pkl", 
            "Ti_stat_3_1.5.pkl", 
            "Ti_stat_3_2.0.pkl" ]



for i in range(len(files1)):
    o = pl.load(open(files1[i],"rb"))
    if(i == 0):
        Ti1 = np.array(o["Ti"])
        xi = o["xi"]
    else:
        Ti1 = np.vstack([Ti1,np.array(o["Ti"]) ])


    tp.temperaturePlot(xi, Ti1, [0.5, 0.72, 0.75, 1.0], [14.23, 0, 0, 0], title = "Temperature Plot for y=0mm, z=0.009mm, geo = 1", num_max=4)



for i in range(len(files2)):
    o = pl.load(open(files2[i],"rb"))
    if(i == 0):
        Ti2 = np.array(o["Ti"])
        xi = o["xi"]
    else:
        Ti2 = np.vstack([Ti2,np.array(o["Ti"]) ])


    tp.temperaturePlot(xi, Ti2, [0.5, 0.72, 0.75, 1.0], [5.5, 6.33, 0, 0], title = "Temperature Plot for y=0mm, z=0.009mm, geo = 2", fig=2, num_max=4)

for i in range(len(files3)):
    o = pl.load(open(files3[i],"rb"))
    if(i == 0):
        Ti3 = np.array(o["Ti"])
        xi = o["xi"]
    else:
        Ti3 = np.vstack([Ti3,np.array(o["Ti"]) ])


    tp.temperaturePlot(xi, Ti3, [0.5, 0.75, 1.0, 1.2, 1.5, 2.0], [2.05, 2.29, 2.48, 0, 0, 0], title = "Temperature Plot for y=0mm, z=0.009mm, geo = 3", fig=3, num_max=6)

cmdInput(locals(), globals())