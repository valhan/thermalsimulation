import numpy as np
import matplotlib.pyplot as plt


def getY(y, yi, height=12):
    if len(yi) == 0:
        return y
    yi_np = np.array(yi)
    yi_np -= y
    yi_np = np.abs(yi_np) - height
    y_closest = np.min(yi_np)
    if y_closest < 0:
        return getY(y+1, yi, height=height) 
    
    return y 

    

def temperaturePlot(xi, Ti,ti, fig=1, height=30, num_max=7):

    ind = Ti[:, 0] != 0

    # a maximum of num_max curves
    if np.sum(ind) > num_max:
        k = np.linspace(0, np.sum(ind) - 1, num_max, dtype=int)
        kk = np.arange(0, len(ind))

        ind = np.isin(kk, k)

        dy = (np.max(Ti[:])-25)/(num_max-1)
    elif np.sum(ind) == 1:
        dy = 0
    else:
        dy = (np.max(Ti[:])-25)/(np.sum(ind)-1)
    # yi = []

    
    plt.figure(fig)
    plt.clf()
    plt.title("Temperature Plot for y=0mm, z=0.009mm")
    j = 0
    for i in range(len(ind)):
        
        if ind[i] == False:
            continue
        
        # maximum value
        rounding = 2
        ind_max = np.argmax(np.floor(Ti[i,:]/rounding)*rounding)
        ind_max2 = np.argmax(np.flip(np.floor(Ti[i,:]/rounding)*rounding, 0))
        
        ind_max = int((ind_max + len(xi) - ind_max2 - 1)/2)
        
        
        plt.plot(xi[:], Ti[i, :])
        #plt.arrow(xi[ind_max], Ti[i, ind_max], max(xi)/2*0.5, 0,, head_width=0.1)

        #yi.append(getY(Ti[i, ind_max], yi, height=height))
        plt.annotate(str(int(Ti[i, ind_max])) + "°C @ " + str((int(ti[i]*1000))) + " ms", 
                        xy=(xi[ind_max], Ti[i, ind_max]), xytext=(max(xi)*0.5, 25 + dy*j), 
                        arrowprops=dict(arrowstyle="->"))
        j +=1
        
    #plt.plot(xi, np.ones(len(xi))*1084, '--k')
    plt.xlabel("x in mm")
    plt.ylabel("T in °C")
    plt.grid(1)

    

    plt.show(0)