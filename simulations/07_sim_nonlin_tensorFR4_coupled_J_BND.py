# import netgen.gui
from myPackage import cmdInput, loadView
import importlib as  il 
from ngsolve import *

import numpy as np
import nonLinCLib as nL


#import netgen.gui
import matplotlib.pyplot  as plt

import sys
import os

SetNumThreads(10)
a = il.import_module("geometry")
tp = il.import_module("temperaturePlot")
# -----------------------------------------------------------------------------
# parameter
# -----------------------------------------------------------------------------
order = 2       # order of Finite Element Space
maxh = 10        # in mm 
outer=[10, 10 , 10] # far boundary

if len(sys.argv) == 1:
    print("arguments: Geometry in{1, 2, 3}; I0 in A; t_end in s; time steps; \n\tdefault: 1 1 3 30")
    input("press Enter")

if len(sys.argv) > 1:
    geometry = int(sys.argv[1]) #[1,2,3]
else:
    geometry = 1



if len(sys.argv) > 2:
    I0 = float(sys.argv[2])
else:
    I0 = 1

if len(sys.argv) > 3:
    t_end = float(sys.argv[3])
else:
    t_end = 3

if len(sys.argv) > 3:
    N_t = int(sys.argv[4])
else:
    N_t = 30



limit_for_open = 1 #% of the narrow track


# -----------------------------------------------------------------------------
# material parameter
# -----------------------------------------------------------------------------
limit_for_open *= 1/100
#cu
rhoCU = 8.92e3
cCU = 385
EMeltCU = 13.05e3
mCU=63.54e-3

sigmaCU_T20 = 58e6
sigmaCU_alpha = 3.93e-3 # with sigma = sigmaCU_T20/(1 + alpha(T - T20))

T_KL = [0, 50, 100, 300, 406, 700, 715, 1083] # °C
lambdaCU_KL = [401, 394, 393, 368, 356, 320, 317, 264] # W/mK

T_melt_CU = 1084.62

# FR4
rhoFR4 = 2e3
cFR4=950
lambdaFR4xy = 0.8
lambdaFR4z = 0.55

# air
lambdaAir = 0.0262
rhoAir = 1.1839
cAir = 1005 

# initial temp 
T_init = 25 #°C


# -----------------------------------------------------------------------------
# mesh generation
# -----------------------------------------------------------------------------
print("------load geometry-------")
#name = "printedFuse"
scale = 1e-3
copper_height=0.018
if geometry == 1:
    length_straight = 0.05
    if not os.path.isfile("geo1.vol"):
        a.firstGeometry(name="geo1", outer=outer, maxh=maxh, scale = scale, copper_height=copper_height)
    mesh = Mesh("geo1.vol")
elif geometry == 2:
    length_straight = 1.05
    if not os.path.isfile("geo2.vol"):
        a.secondGeometry(name="geo2", outer=outer, maxh=maxh, scale = scale, copper_height=copper_height)
    mesh = Mesh("geo2.vol")
else:
    length_straight = 1.05
    if not os.path.isfile("geo3.vol"):
        a.thirdGeometry(name="geo3", outer=outer, maxh=maxh, scale = scale, copper_height=copper_height)
    mesh = Mesh("geo3.vol")

#a.meanderGeometry(name=name, outer=[10, 10 , 10], length_straight=3, NumMeander=10, meander_copper_pad_width=2, meander_copper_pad_length=0, maxh=1)
#mesh = Mesh(name + ".vol")

Draw(CoefficientFunction([0, 1, 2]), mesh, "mat", draw_surf=False)

bound_val = {"CUleft": 4, "CUright":3, "airleft":9, "airright":6, "airtop":7, "airbottom":1, "airback":2, "airfront":5 }
bound = CoefficientFunction([ bound_val[bc] if bc in bound_val.keys() else 0 for bc in mesh.GetBoundaries() ])
#print ("mu_coef=", mu_coef)
Draw(bound,mesh,'boundaries', draw_surf=True )


maskCU = CoefficientFunction([0, 0, 1])
Draw(maskCU, mesh, "maskCU")


#mip = mesh(0, 0, 0.18/2 * scale)
# ++++++++++++++++++++++
# VSpace electric
# ++++++++++++++++++++++
fesE = H1(mesh, order = order, dirichlet="CUright", definedon=mesh.Materials("CU"))

# ++++++++++++++++++++++
# VSpace thermal
# ++++++++++++++++++++++
fesT = H1(mesh, order = order, dirichlet="CUleft|CUright")
VSpace = FESpace([fesE, fesT])

sol = GridFunction(VSpace, "sol")
sol_old = GridFunction(VSpace, "sol_old")
sol_it_old = GridFunction(VSpace, "sol_it_old")
sol_BND = GridFunction(VSpace, "sol_BND")


phi = sol.components[0]
phi_old = sol_old.components[0]
phi_it_old = sol_it_old.components[0]

TR = sol.components[1]
TR_old = sol_old.components[1]
TR_it_old = sol_it_old.components[1]

uE, uT = VSpace.TrialFunction()
vE, vT = VSpace.TestFunction()


T_full = TR + T_init

melting = IfPos(T_full - T_melt_CU, 1, 0)
T = IfPos(melting, T_melt_CU, T_full)
T_melting = IfPos(melting, T_full - T_melt_CU , 0)


q = -grad(TR)

res = sol.vec.CreateVector()

Draw(T, mesh, "T")
Draw(q, mesh, "q")


# ++++++++++++++++++++++
# parameters Energy
# ++++++++++++++++++++++

Energy_melting = cCU * rhoCU * (T_melting) * maskCU
Energy_melted = EMeltCU/mCU * rhoCU

mask_fuse = IfPos(x + length_straight/2*scale, 1, 0) * IfPos(x - length_straight/2*scale, 0, 1)*maskCU

melted = IfPos((Energy_melting - Energy_melted)*mask_fuse, 1, 0) * 1/Integrate(mask_fuse, mesh)

Draw(melted, mesh, "melted")
Draw(melting, mesh, "melting")
# ++++++++++++++++++++++
# parameters electric
# ++++++++++++++++++++++

J0 = I0/(copper_height * scale * 4 * scale)


intrule = IntegrationRule(TET, order*2)

T_KL_CU = np.linspace(0, 1000, 100)
sigma_KL_CU = sigmaCU_T20/(1 + sigmaCU_alpha*(T_KL_CU - 20))
sigmaCU = nL.NonLinCF(mesh, intrule, T, T_KL_CU, sigma_KL_CU, mask=maskCU)



sigmaCU_FP = 1/2*(np.min(sigma_KL_CU) + np.max(sigma_KL_CU))


E = -grad(phi)
J = sigmaCU * E

# calc losses
not_melted_parameter = Parameter(1)
p_elec =  InnerProduct(E, J)* not_melted_parameter

Draw(E, mesh, "E")
Draw(J, mesh, "J")
Draw(phi, mesh, "phi")
Draw(p_elec, mesh, "Losses")
Draw(CoefficientFunction([0, 0, sigmaCU]), mesh, "sigmaCU", draw_surface=False)

# ++++++++++++++++++++++
# parameters thermal
# ++++++++++++++++++++++
lambdaFR4 = CoefficientFunction((lambdaFR4xy, 0, 0, 0, lambdaFR4xy, 0, 0, 0, lambdaFR4z))
lambdaFR4.dims = (3, 3)

lambdaCU = nL.NonLinCF(mesh, intrule, T, T_KL, lambdaCU_KL, mask=maskCU)


c = CoefficientFunction([cAir, cFR4, cCU])
rho = CoefficientFunction([rhoAir, rhoFR4, rhoCU])

dmin = np.min(lambdaCU_KL)
dmax = np.max(lambdaCU_KL)
lambdaFP = (dmax+dmin)/2


Draw(CoefficientFunction([0, 0, lambdaCU]), mesh, "lambdaCU", draw_surface=False)


# ++++++++++++++++++++++
# parameters time
# ++++++++++++++++++++++
ti = np.linspace(0, t_end, N_t)
dt = t_end/(N_t-1)

has_melted = False



with TaskManager():

       # ++++++++++++++++++++++
    # BFI and LFI electric
    # ++++++++++++++++++++++
    a = BilinearForm(VSpace, symmetric=True)
    a += SymbolicBFI(sigmaCU_FP*grad(uE)*grad(vE))
    
    
    

    # ++++++++++++++++++++++
    # BFI and LFI thermal
    # ++++++++++++++++++++++
    a += SymbolicBFI((lambdaFR4*grad(uT))*grad(vT), definedon=mesh.Materials("FR4"))
    a += SymbolicBFI(lambdaAir*grad(uT)*grad(vT), definedon=mesh.Materials("air"))
    a += SymbolicBFI(lambdaFP*grad(uT)*grad(vT), definedon=mesh.Materials("CU"))
    a.Assemble()

    m = BilinearForm(VSpace, symmetric=True)
    m += SymbolicBFI(rho*c*uT*vT)
    m.Assemble()


    # ++++++++++++++++++++++
    # solve
    # ++++++++++++++++++++++
    # Au + d/dt Mu = f
    # Au + M(u - u_old)/dt = f
    # dtAu + Mu - Mu_old = dtf
    # dtAu + Mu = dtf + Mu_old
    # u = (dtA + M)\(dtf + Mu_old)


    mstar = m.mat.CreateMatrix()
    mstar.AsVector().data = dt * a.mat.AsVector() + m.mat.AsVector() 

    invmstar = mstar.Inverse(freedofs=VSpace.FreeDofs(), inverse="sparsecholesky")
    
    # right hand side 
    f = LinearForm(VSpace)
    f += SymbolicLFI((sigmaCU_FP - sigmaCU)*phi_old * vE, definedon=mesh.Materials("CU"))
    f += SymbolicLFI(p_elec * vT, definedon=mesh.Materials("CU"))
    f += SymbolicLFI((lambdaFP - lambdaCU)*TR_old * vT, definedon=mesh.Materials("CU"))

    # Neumann Boundary for electrical problem
    f += SymbolicLFI(J0 * vE, definedon=mesh.Boundaries("CUleft"))
    

    



# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# ------------------------------------------------------------------------------
# -----------   solve transient field problem
# ------------------------------------------------------------------------------
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++



loadView()


xi = np.linspace(-4.5, 4.5, 5000) * scale
T_ti = np.zeros([len(ti), len(xi)])

T_xx = []
melted_i = []



print("-------------------Start transient simulation------------------")

time_melted = -1
T_melted_sim = -1
I_max = -1
with TaskManager():

    for i in range(N_t):
        print("########### step "  + str(i) +" / " + str(N_t - 1) +" ti:\t" + str(ti[i])+ "###########")



        sol_old.vec.data = sol.vec
        #TR.Set(0, definedon=mesh.Boundaries("CUleft|CUright"))
        val = {"CUright":0}
        bnd_val = CoefficientFunction([val[key] if key in val.keys() else 0 for key in mesh.GetBoundaries()])
        sol_BND.components[0].Set(bnd_val, definedon=mesh.Boundaries("CUright"))
        sol_BND.components[1].Set(0, definedon=mesh.Boundaries("CUleft|CUright"))
        

        

        sigmaCU.Update()
        lambdaCU.Update()


        it = 0
        while True:

            it += 1
            print ("Inner Iteration",it)
            
            sol_it_old.vec.data = sol.vec

            
            # solve electric problem
            f.Assemble()
            sol.components[0].vec.data = sol_BND.components[0].vec
            sol.components[1].vec.data = sol_BND.components[1].vec
            # solve thermal problem
            res.data = dt*f.vec + m.mat*sol_old.vec - mstar*sol_BND.vec
            sol.vec.data += invmstar * res
            

            sigmaCU.Update()
            lambdaCU.Update()


            errL2T=Integrate(InnerProduct((TR - TR_it_old), (TR - TR_it_old))				
                                ,mesh, definedon=mesh.Materials("CU"))
                                
            solL2T=Integrate(InnerProduct(TR, TR),mesh, definedon=mesh.Materials("CU"))
    

            errL2E=Integrate(InnerProduct((phi - phi_it_old), (phi - phi_it_old))				
                                ,mesh, definedon=mesh.Materials("CU"))
                                
            solL2E=Integrate(InnerProduct(phi, phi),mesh, definedon=mesh.Materials("CU"))

            # if solL2 > 0:
            #     print("err:",errL2/solL2)
            #     print("solL2:",solL2)
            #     print("errL2:",errL2)
            if it == 1:
                continue


            if solL2E == 0 or solL2T ==0:
                break
            if abs(errL2T/solL2T) + abs(errL2E/solL2E) < 1e-5:
                # print("Iterations:",it)
                break
                
                
            if it==50:
                print("warning: too many iterations", it)
                wentWrong=True
                # print("managed accuracy",errL2/solL2)
                break
        



        T_ti[i, :] = T(mesh(xi, 0, copper_height/2*scale)).transpose()[0]
        T_xx.append(T(mesh(0, 0, copper_height/2*scale)))
        melted_i.append(Integrate(melted, mesh))

        if True and i % 30 == 0 or i == N_t - 1:

            # tp.temperaturePlot(xi, T_ti, ti, height = 20)

            plt.figure(2)
            plt.clf()
            plt.title("Transient Simulation for I="+ str(I0)+", geo=" +str(geometry))
            plt.plot(np.hstack([0, ti[:i+1]]), [25] + T_xx)
            plt.xlabel("time in s")
            plt.ylabel("temperature in °C")
            plt.grid(1)


            # plt.figure(3)
            # plt.clf()
            # plt.title("Transient Simulation for U0="+ str(U0)+", geo=" +str(geometry))
            # plt.plot(ti[:i+1], melted_i)
            # plt.xlabel("time in s")
            # plt.ylabel("melted material in %")
            # plt.grid(1)
            # plt.show(0)
            plt.pause(0.1)
            # Redraw()

        if not has_melted:
            has_melted = Integrate(melted, mesh) > limit_for_open 
        if(has_melted and  not_melted_parameter.Get() == 1):
            print("********************Copper has melted*********************")
            time_melted = ti[i]
            T_melted_sim = T(mesh(0, 0, copper_height/2*scale))

            P = Integrate(p_elec, mesh)
            U_max = P/I0
            not_melted_parameter.Set(0)      
            #cmdInput(locals(), globals()) 
  



# -------------------- post processing ----------------


# --- calc current ----
# eps = 0.5*scale
# I_vol = IfPos(x + (4.5*scale), 1, 0) * IfPos(x + (4.5*scale) - eps, 0, 1) * maskCU
# I = Integrate(J[0]*I_vol, mesh)/Integrate(I_vol, mesh)* (copper_height*scale * 4*scale)

print("------------------- finished transient simulation------------------")


print("used geometry:\t", geometry)
# print("U0:\t", U0)


if has_melted:
    print("        has melted")
    print("        time when melted:\t", time_melted, "s")
    print("        current:\t", 0, "A")
    print("        maximum current:\t", I0, "A")
    print("        maximum voltage:\t", U_max, "V")
    print("        Ohm Losses @ break_through:\t", P)
else:
    P = Integrate(p_elec, mesh)
    # R = U0**2/P
    U_max = P/I0
    
    print("------- has NOT melted")
    print("------- time when melted:\t", t_end, "s")
    print("------- current:\t", I0, "A")
    print("------- voltage:\t", U_max, "V")
    print("------- Ohm Losses:\t", P)
    

print("how much has melted?\t", Integrate(melted, mesh)*100, "%")
print(geometry, "&", np.round(U_max, 2), "&",np.round(I0, 2), "&",np.round(P,2), "&",has_melted ,"(",np.round(time_melted*1000,2), ")&", np.round(t_end*1000, 2), "\\\\\hline" )

import pickle as pl
pl.dump({"Ti":T_ti, "T_xx":T_xx, "xi":xi, "P":P, "melted":has_melted, "ti":ti , "U_max":U_max, "time_melted":time_melted, "I0":I0, "t_end":t_end, 
            "N_t":N_t}, 
            open("./save/trans/Ti_trans_"+str(geometry) + "_I_"+str(I0)+".pkl", "wb"))


cmdInput(locals(), globals())
