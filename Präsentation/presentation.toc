\babel@toc {ngerman}{}
\beamer@sectionintoc {1}{\IeC {\"U}berblick}{2}{0}{1}
\beamer@sectionintoc {2}{Modelle von Track Fuses}{3}{0}{2}
\beamer@sectionintoc {3}{W\IeC {\"a}rme- leitungs- gleichung}{9}{0}{3}
\beamer@sectionintoc {4}{Elektrisches Feld}{10}{0}{4}
\beamer@sectionintoc {5}{Coupled Problem}{11}{0}{5}
\beamer@sectionintoc {6}{Simulations- ergebnisse}{13}{0}{6}
\beamer@sectionintoc {7}{Future Work}{16}{0}{7}
